package br.com.softbox.exemplos.xls;

import java.io.IOException;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

@WebServlet("/importar-planilha-exemplo")
public class ImportarPlanilhaExemploServlet extends ImportarPlanilhaServlet {

	private static final long serialVersionUID = 1L;

	protected void importarPlanilha(HSSFWorkbook wb, HttpServletResponse resp)
			throws IOException {
		HSSFCell cell = wb.getSheetAt(0).getRow(0).getCell(0);
		resp.getWriter().write(
				"<h1>Planilha importada com sucesso!</h1> <br> Valor encontrado em A1: "
						+ cell.getRichStringCellValue().getString() + "!");
	}

}
