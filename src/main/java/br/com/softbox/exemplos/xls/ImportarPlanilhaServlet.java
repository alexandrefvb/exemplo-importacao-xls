package br.com.softbox.exemplos.xls;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItemIterator;
import org.apache.commons.fileupload.FileItemStream;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public abstract class ImportarPlanilhaServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {

		if (!ServletFileUpload.isMultipartContent(req)) {
			resp.sendError(400, "Requisição inválida!");
		}

		ServletFileUpload upload = new ServletFileUpload();

		try {
			FileItemIterator fileItemIterator = upload.getItemIterator(req);
			while (fileItemIterator.hasNext()) {
				FileItemStream fileItem = fileItemIterator.next();
				if (!fileItem.isFormField()) {
					HSSFWorkbook wb = new HSSFWorkbook(fileItem.openStream());
					importarPlanilha(wb, resp);
				}
			}
		} catch (FileUploadException e) {
			resp.sendError(500, "Erro ao receber planilha!");
		}
	}

	protected abstract void importarPlanilha(HSSFWorkbook wb,
			HttpServletResponse resp) throws IOException;
}
